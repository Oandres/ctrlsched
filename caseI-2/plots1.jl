#  This Julia file generates the plots with the results obtained
#   by executing the main file

using PyPlot

close("all")
# Time vector
t = Array{Float64}(undef,N*M)
for i in 1:N
	for j in 1:M
      if i == 1
         tpre = 0
      else
         tpre = t[M*(i-1)]
      end
      t[j+M*(i-1)] = τ[j]*hs[i] + tpre
	end
end

# ========= The values at the beginning of each finite element are added ========
# - New Arrays to allocate the values
Us  = Array{Float64}(undef,N*M+N)
TTs = Array{Float64}(undef,N*M+N)
Z1s = Array{Float64}(undef,N*M+N)
Z2s = Array{Float64}(undef,N*M+N)
Ss  = Array{Float64}(undef,N*M+N-1)
x1s = Array{Float64}(undef,N*M+N)
x2s = Array{Float64}(undef,N*M+N)
x3s = Array{Float64}(undef,N*M+N)

# S domain 
for i in 2:N
   Ss[(i-1)*M+(i-1)] = t[(i-1)*M]
end
for i in ni
   for j in mj
      Ss[(M+1)*(i-1)+j] = t[M*(i-1)+j]
   end
end
pushfirst!(Ss,0.0)

# - The values at the beginning of each finite element are allocated first
for i in ni
   Us[(i-1)*M+i] = u0s[i]
   Z1s[(i-1)*M+i] = z10s[i]
   Z2s[(i-1)*M+i] = z20s[i]
   TTs[(i-1)*M+i] = tt0s[i]
   x1s[(i-1)*M+i] = X10s[i]
   x2s[(i-1)*M+i] = X20s[i]
   x3s[(i-1)*M+i] = X30s[i]
end

# - then the values at the all collocation points are added
for i in ni
   for j in mj
      Us[M*(i-1)+i+j]  = us[M*(i-1)+j]
      Z1s[M*(i-1)+i+j] = z1s[M*(i-1)+j]
      Z2s[M*(i-1)+i+j] = z2s[M*(i-1)+j]
      TTs[M*(i-1)+i+j] = tts[M*(i-1)+j]
      x1s[M*(i-1)+i+j] = X1s[M*(i-1)+j]
      x2s[M*(i-1)+i+j] = X2s[M*(i-1)+j]
      x3s[M*(i-1)+i+j] = X3s[M*(i-1)+j]
   end
end
# ================================================================


plt1 = subplots()
subplot(221)
plot(Ss,Z1s,linewidth=2)
xlabel(L"s",fontsize=14)
ylabel(L"C",fontsize=14)
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)

subplot(222)
plot(Ss,Z2s,linewidth=2)
xlabel(L"s",fontsize=14)
ylabel(L"T",fontsize=14)
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)

subplot(223)
plot(t,IL1s,t,IL2s,t,IL3s,linewidth=2)
xlabel(L"s",fontsize=14)
legend([L"I_1",L"I_2",L"I_3"],fontsize=12)
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)

subplot(224)
plot([0;Ss],[Us[end];Us],linewidth=2)
xlabel(L"s",fontsize=14)
ylabel(L"u",fontsize=14)
axhline(y=uU,xmin=0,linewidth=1.5,linestyle="--")
axhline(y=uL,xmin=0,linewidth=1.5,linestyle="--")
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)

tight_layout()
savefig("SolutionS.pdf")
