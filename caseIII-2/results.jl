# Auxiliary file to read the optimal result from the solution
z1s = transpose(value.(z1).data)[:]
z2s = transpose(value.(z2).data)[:]
z3s = transpose(value.(z3).data)[:]
z4s = transpose(value.(z4).data)[:]
z5s = transpose(value.(z5).data)[:]
z6s = transpose(value.(z6).data)[:]
z7s = transpose(value.(z7).data)[:]
X1s = transpose(value.(X).data[1,:,:])[:]
X2s = transpose(value.(X).data[2,:,:])[:]
X3s = transpose(value.(X).data[3,:,:])[:]
X4s = transpose(value.(X).data[4,:,:])[:]
IL1s = transpose(value.(IL1).data)[:]
IL2s = transpose(value.(IL2).data)[:]
IL3s = transpose(value.(IL3).data)[:]
IL4s = transpose(value.(IL4).data)[:]
z10s = value.(z10).data
z20s = value.(z20).data
z30s = value.(z30).data
z40s = value.(z40).data
z50s = value.(z50).data
z60s = value.(z60).data
z70s = value.(z70).data
X10s = value.(X0).data[1,:]
X20s = value.(X0).data[2,:]
X30s = value.(X0).data[3,:]
X40s = value.(X0).data[4,:]
IL10s = value.(IL10).data
IL20s = value.(IL20).data
IL30s = value.(IL30).data
IL40s = value.(IL40).data
tt0s = value.(tt0).data
u1s  = transpose(value.(u1).data)[:]
u2s  = transpose(value.(u2).data)[:]
u10s = value.(u10).data
u20s = value.(u20).data
ys   = transpose(value.(y).data)[:]
vs   = value.(v).data
tts  = transpose(value.(tt).data)[:]
μbs  = z4s./z3s
Jschs = value(Jsch)
Jdyns = value(Jdyn)
Jpens = value(Jpen)
Jtot  = Jschs-Jdyns
tsw1 = value(tt[Ni[1][end],M])
tsw2 = value(tt[Ni[2][end],M])
tsw3 = value(tt[Ni[3][end],M])
cputime = solve_time(m)
println("Jsch: ",Jschs)
println("Jdyn: ",Jdyns)
println("Jpen: ",Jpens)
println("Jtot: ",Jtot)
println("Final time = ",tts[end])
println("Switching times : ")
println("t1: ",tsw1)
println("t2: ",tsw2)
println("t3: ",tsw3)
println("CPU time = ",cputime)
open("solution.txt","w") do g
   write(g,"Jsch = $Jschs\n")
   write(g,"Jdyn = $Jdyns\n")
   write(g,"Jtot = $Jtot\n")
   write(g,"Final time = $(tts[end])\n")
   write(g,"Switching times : \n")
   write(g,"t1: $tsw1\n")
   write(g,"t2: $tsw2\n")
   write(g,"t3: $tsw3\n")
   write(g,"CPU time: $cputime\n")
end 
