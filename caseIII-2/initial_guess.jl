# This julia file generates initial guess values for z,u,X and y based on
# the sequence Seq0 

Seqi = Dict{Int64,Int64}()
for p in np
   Seqi[p] = Seq0[p]
end

Seqj = sort(collect(Seqi),by=x->x[1])

Z10 = Array{Float64}(undef,N,M)
Z20 = Array{Float64}(undef,N,M)
Z30 = Array{Float64}(undef,N,M)
Z40 = Array{Float64}(undef,N,M)
Z50 = Array{Float64}(undef,N,M)
Z60 = Array{Float64}(undef,N,M)
U10 = Array{Float64}(undef,N,M)
U20 = Array{Float64}(undef,N,M)
for q in nq
   k = Seqj[q][2]
   Z10[Ni[q],:] .= z1p[k]
   Z20[Ni[q],:] .= z2p[k]
   Z30[Ni[q],:] .= z3p[k]
   Z40[Ni[q],:] .= z4p[k]
   Z50[Ni[q],:] .= z5p[k]
   Z60[Ni[q],:] .= z6p[k]
   U10[Ni[q],:] .= u1p[k]
   U20[Ni[q],:] .= u2p[k]
end

x0 = Array{Float64}(undef,P,N,M)
x0 .= 0.0
for q in nq
   k = Seqj[q][2]
   x0[k,Ni[q][1]:end,:] .= D[k]
end   

y0 = Array{Float64}(undef,P,Q)
y0 .= -1.0
for q in nq
   k = Seqj[q][2]
   y0[k,q] = 1.0
end  



