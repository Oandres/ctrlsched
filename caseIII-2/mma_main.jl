# This Julia file contains the solution of the integrated optimal control
# and scheduling case study III: Methyl methacrylate (MMA) process
#
# Scenario 2: Due dates on products 1 and 4
#
# Oswaldo Andrés-Martínez
# oswxandres@gmail.com
# Jan 2021
#
# Notes: There are changes in the nomenclature with respect to the paper
#        i and j are indices for finite elements and collocation points resp.
#        q is the index of subintervals
#        X's are the cumulative volume of each product   
# Updated Feb 2021

using JuMP, Ipopt, PyPlot

# - Steady states determined by simulations
μp = [43707 54490 76349 96973]
z1p = [5.767 5.83 5.90 5.94]
z2p = [0.216 0.136 0.056 0.024]
z3p = [5.34e-4 3.12e-4 1.29e-4 6.32e-5]
z4p = [23.354 16.996 9.839 6.132]
z5p = [329.91 328.83 327.56 326.95]
z6p = [296.40 296.55 296.62 296.77]
u1p = [0.027 0.017 0.007 0.003]
u2p = [3.6 3.3 3.1 2.9]

Xin = [0 0 0 0] # Initial amount of products (m3)

# Model parameters
# - Kinetic parameters
Ztc = 3.8223e10
Ztd = 3.1457e11
Zi  = 3.7920e18
Zp  = 1.7700e9
Zfm = 1.0067e15
fs  = 0.58
Etc = 2.9442e3
Etd = 2.9442e3
Ei  = 1.2877e5
Ep  = 1.8283e4
Efm = 7.4478e4

# - System parameters
z1i0 = 6.4678
z2i0 = 8.0
z5i0 = 350
V  = 0.1
ρ1 = 866
cp = 2.0
A  = 2.0
ρ2 = 1000
V0 = 0.02
R  = 8.314
Mm = 100.12
ΔH = 57800 # (-ΔH)
U  = 720
cw = 4.2
Tw0 = 293.2
qf = 1      # Feed flow rate 

# Bounds on the control
u1L = 0.001; u1U = 0.04
u2L = 1.0;   u2U = 4.0

# - Scheduling parameters
Dkg = [9550 12220 13220 14150]  # Demand of products (kg)
D = Dkg./ρ1                     # Demand of products (m3)
Cpkg = [0.9 1.2 1.3 1.5]        # Prices of each product ($/kg)
Cp = Cpkg.*ρ1                   # Prices of each product ($/m3)
Cskg = [0.010 0.011 0.012 0.05] # Storage costs ($/kgh)
Cs = Cskg.*ρ1                   # Storage costs ($/m3h)
Cr = 1.5 
td1 = 48.0 # Due date 1
td4 = 24.0 # Due date 4

# Other parameters
δ = 1e3 # Penalty parameter
st = [2 2 2 2]
# - Weights for each term in the optimal control objective function
α1 = 0
α2 = 0
α3 = 0
α4 = 0
α5 = 0
α6 = 0
α7 = 100
α8 = 1

# Size parameters
N = 16  # Number of finite elements
M = 2   # Number of collocation points
P = 4   # Number of products
Q = 4   # Number of intervals

# Collocation matrix and points and quadrature weights
# (Radau points)
include("Rtableau.jl")
Ω,ω,τ = butcher(M+1)
# Lagrange coefficients for interpolation
include("Lagrange.jl")
lag = Lagrange(0,τ)

# Sets
np = 1:P  # Products
nq = 1:Q  # Intervals
ni = 1:N
Ni = Array{UnitRange}(undef,Q)  # Subintervals set
for i in 1:Q
   Ni[i] = ((i-1)*Int(N/Q) + 1):i*Int(N/Q)
end
mj = 1:M  # Collocation points
mk = copy(mj)

# Initial guess 
# - Guessed sequence
Seq0 = [4 2 1 3]
include("initial_guess.jl")

# Generate the nlp 
include("mma_dd.jl")

# Solution
optimize!(m)

# Reading results
hs = value.(h).data
include("results.jl")

# Plots in the domain s
include("plots1.jl")
# Plots in the domain t
include("plots2.jl")
