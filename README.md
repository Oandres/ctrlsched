# ctrlsched

This repository contains the Julia files of the paper: "A switched system formulation for optimal integration of scheduling and control in multi-product continuous processes" - Oswaldo Andrés-Martínez, Luis A. Ricardez-Sandoval.

Packages: JuMP, Ipopt, PyPlot

Folder auxfiles contains the files needed to run all the programs, they should be located in the working directory. If solver MA27 is not available, ipopt.opt file can be removed so that the default linear solver is used, though the results and CPU times may be somewhat different from those in the paper.

To run the programs, it is necessary only to run the file "name"_main.jl, where "name" is the name of the case study: cstr, hicks, mma.

All programs were tested in Julia 1.5.3.
