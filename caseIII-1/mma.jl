# This Julia file contains the solution of the integrated optimal control
# and scheduling case study III: Methyl methacrylate (MMA) process
#
# Scenario 1: No due dates
#
# Oswaldo Andrés-Martínez
# oswxandres@gmail.com
# Jan 2021

# Model definition
m = Model(Ipopt.Optimizer)

# Variables
@variable(m,z1[i in ni,j in mj],start=Z10[i]) 
@variable(m,0.001<=z2[i in ni,j in mj]<=0.5,start=Z20[i])   
@variable(m,1e-5<=z3[i in ni,j in mj]<=1e-3,start=Z30[i])           
@variable(m,1.0<=z4[i in ni,j in mj]<=50,start=Z40[i]) 
@variable(m,z5[i in ni,j in mj],start=Z50[i])   
@variable(m,z6[i in ni,j in mj],start=Z60[i])   
@variable(m,z7[i in ni,j in mj])
@variable(m,v[q in nq]>=0,start=10)
@variable(m,0<=X[p in np,i in ni,j in mj]<= 1.1D[p],start=x0[p,i,j])  
@variable(m,z10[i in ni],start=Z10[i])
@variable(m,0.001<=z20[i in ni]<=0.5,start=Z20[i])
@variable(m,1e-5<=z30[i in ni]<=1e-3,start=Z30[i])
@variable(m,1.0<=z40[i in ni]<=50,start=Z40[i])
@variable(m,z50[i in ni],start=Z50[i])
@variable(m,z60[i in ni],start=Z60[i])
@variable(m,z70[i in ni])
@variable(m,-1<=y[p in np,q in nq]<=1,start=y0[p,q])
@variable(m,X0[p in np,i in ni]>=0,start=x0[p,i,1])
@variable(m,tt[i in ni,j in mj]>=0)
@variable(m,tt0[i in ni]>=0)
@variable(m,u1L <= u1[i in ni,j in mj] <= u1U,start=U10[i])
@variable(m,u2L <= u2[i in ni,j in mj] <= u2U,start=U20[i])
@variable(m,0.05Q/N <= h[i in ni] <= 2Q/N,start=Q/N)
# ================  Auxiliary expressions ============
# - Penalty function
function γp(x)
   return log(0.5,x)
end
register(m,:γp,1,γp,autodiff=true)

# - MWD
@NLexpression(m,μb[i in ni,j in mj],z4[i,j]/z3[i,j])

# - P0
@NLexpression(m,P0[i in ni,j in mj],
   (2*fs*z2[i,j]*Zi*exp(-Ei/R/z5[i,j])/(Ztd*exp(-Etd/R/z5[i,j])
                                     +Ztc*exp(-Etc/R/z5[i,j])))^(0.5))

# - Deviation from the steady state
@NLexpression(m,SS[p in np,i in ni,j in mj],α1*(z1p[p]-z1[i,j])^2 
  + α2*(z2p[p]-z2[i,j])^2 + α3*(z3p[p]-z3[i,j])^2 + α4*(z4p[p]-z4[i,j])^2 
  + α5*(z5p[p]-z5[i,j])^2 + α6*(z6p[p]-z6[i,j])^2 + α7*(u1p[p]-u1[i,j])^2 
                                                  + α8*(u2p[p]-u2[i,j])^2)

# - RHS of the state equations
@NLexpression(m,f1[i in ni,j in mj],
 -(Zp*exp(-Ep/R/z5[i,j])+Zfm*exp(-Efm/R/z5[i,j]))*z1[i,j]*P0[i,j] + qf*(z1i0-z1[i,j])/V)
   
@NLexpression(m,f2[i in ni,j in mj],-Zi*exp(-Ei/R/z5[i,j])*z2[i,j] 
                                       + (u1[i,j]*z2i0-qf*z2[i,j])/V)

@NLexpression(m,f3[i in ni,j in mj],
   (0.5*Ztc*exp(-Etc/R/z5[i,j])+Ztd*exp(-Etd/R/z5[i,j]))*P0[i,j]^2 
      + Zfm*exp(-Efm/R/z5[i,j])*z1[i,j]*P0[i,j] - qf*z3[i,j]/V)

@NLexpression(m,f4[i in ni,j in mj],
   Mm*(Zp*exp(-Ep/R/z5[i,j])+Zfm*exp(-Efm/R/z5[i,j]))*z1[i,j]*P0[i,j] - qf*z4[i,j]/V)

@NLexpression(m,f5[i in ni,j in mj],P0[i,j]*Zp*exp(-Ep/R/z5[i,j])*z1[i,j]*ΔH/ρ1/cp
   - U*A*(z5[i,j]-z6[i,j])/ρ1/cp/V + qf*(z5i0 - z5[i,j])/V)

@NLexpression(m,f6[i in ni,j in mj],u2[i,j]*(Tw0-z6[i,j])/V0 
                                            + U*A*(z5[i,j]-z6[i,j])/ρ2/cw/V0)

@NLexpression(m,f7[q in nq,i in Ni[q],j in mj],
                sum(((y[p,q]+1)/2)*SS[p,i,j] for p in np))

@NLexpression(m,f8[p in np,q in nq,i in ni,j in mj],(v[q]*(y[p,q]+1)/2)*qf)

# - Interpolation of algebraic variables
@NLexpression(m,u10[i in ni],sum(lag[j]*u1[i,j] for j in mj))
@NLexpression(m,u20[i in ni],sum(lag[j]*u2[i,j] for j in mj))

# - Collocation equations
#   RHS of the state equations in the domain s
@NLexpression(m,z1dot[q in nq,i in Ni[q],j in mj],v[q]*f1[i,j])
@NLexpression(m,z2dot[q in nq,i in Ni[q],j in mj],v[q]*f2[i,j])
@NLexpression(m,z3dot[q in nq,i in Ni[q],j in mj],v[q]*f3[i,j])
@NLexpression(m,z4dot[q in nq,i in Ni[q],j in mj],v[q]*f4[i,j])
@NLexpression(m,z5dot[q in nq,i in Ni[q],j in mj],v[q]*f5[i,j])
@NLexpression(m,z6dot[q in nq,i in Ni[q],j in mj],v[q]*f6[i,j])
@NLexpression(m,z7dot[q in nq,i in Ni[q],j in mj],v[q]*f7[q,i,j])
@NLexpression(m,Xdot1[p in np,q in nq,i in Ni[q],j in mj;i<Ni[q][st[q]]+1],0)
@NLexpression(m,Xdot2[p in np,q in nq,i in Ni[q],j in mj;i>=Ni[q][st[q]]+1],f8[p,q,i,j])

#   Equation for t(s)
@NLexpression(m,tdot[q in nq,i in Ni[q],j in mj],v[q])

# - Expresions for the objective function
@NLexpression(m,Jp[q in nq,i in Ni[q],j in mj],sum(δ*γp(y[p,q]^2) for p in np))
@NLexpression(m,Jpint[q in nq],sum(h[i]*ω[j]*v[q]*Jp[q,i,j] for i in Ni[q],j in mj))
@NLexpression(m,Jpen,sum(Jpint[q] for q in nq))

@NLexpression(m,Jsch,sum(Cp[p]*X[p,N,M] for p in np) - qf*Cr*tt[N,M]
   - sum(Cs[p]*X[p,N,M]*((y[p,q]+1)/2)*(tt[N,M]-tt[Ni[q][end],M]) 
                for p in np,q in nq))

@NLexpression(m,Jdyn,sum(z7[Ni[q][end],M] for q in nq))
# =====================================================================

# Objective function
@NLobjective(m,Max,Jsch - Jdyn - Jpen)

# Constraints
# - RK representation of the state and adjoint equations
@NLconstraint(m,h1[q in nq,i in Ni[q],j in mj],
      z1[i,j] == z10[i]+h[i]*sum(Ω[j,k]*z1dot[q,i,k] for k in mk))
@NLconstraint(m,h2[q in nq,i in Ni[q],j in mj],
      z2[i,j] == z20[i]+h[i]*sum(Ω[j,k]*z2dot[q,i,k] for k in mk))
@NLconstraint(m,h3[q in nq,i in Ni[q],j in mj],
      z3[i,j] == z30[i]+h[i]*sum(Ω[j,k]*z3dot[q,i,k] for k in mk))
@NLconstraint(m,h4[q in nq,i in Ni[q],j in mj],
      z4[i,j] == z40[i]+h[i]*sum(Ω[j,k]*z4dot[q,i,k] for k in mk))
@NLconstraint(m,h5[q in nq,i in Ni[q],j in mj],
      z5[i,j] == z50[i]+h[i]*sum(Ω[j,k]*z5dot[q,i,k] for k in mk))
@NLconstraint(m,h6[q in nq,i in Ni[q],j in mj],
      z6[i,j] == z60[i]+h[i]*sum(Ω[j,k]*z6dot[q,i,k] for k in mk))
@NLconstraint(m,h7[q in nq,i in Ni[q],j in mj],
      z7[i,j] == z70[i]+h[i]*sum(Ω[j,k]*z7dot[q,i,k] for k in mk))
@NLconstraint(m,h81[p in np,q in nq,i in Ni[q],j in mj;i<Ni[q][st[q]]+1],
      X[p,i,j] == X0[p,i]+h[i]*sum(Ω[j,k]*Xdot1[p,q,i,k] for k in mk))
@NLconstraint(m,h82[p in np,q in nq,i in Ni[q],j in mj;i>=Ni[q][st[q]]+1],
      X[p,i,j] == X0[p,i]+h[i]*sum(Ω[j,k]*Xdot2[p,q,i,k] for k in mk))
@NLconstraint(m,h9[q in nq,i in Ni[q],j in mj],
      tt[i,j] == tt0[i]+h[i]*sum(Ω[j,k]*tdot[q,i,k] for k in mk))

# - Continuity
@NLconstraint(m,c1[i in 2:N],z10[i] == z1[i-1,M])
@NLconstraint(m,c2[i in 2:N],z20[i] == z2[i-1,M])
@NLconstraint(m,c3[i in 2:N],z30[i] == z3[i-1,M])
@NLconstraint(m,c4[i in 2:N],z40[i] == z4[i-1,M])
@NLconstraint(m,c5[i in 2:N],z50[i] == z5[i-1,M])
@NLconstraint(m,c6[i in 2:N],z60[i] == z6[i-1,M])
@NLconstraint(m,c7[i in 2:N],z70[i] == z7[i-1,M])
@NLconstraint(m,c8[p in np,i in 2:N],X0[p,i] == X[p,i-1,M])
@NLconstraint(m,c9[i in 2:N],tt0[i] == tt[i-1,M])

# - Boundary values
@NLconstraint(m,z1iv,z10[1] == z1[N,M])
@NLconstraint(m,z2iv,z20[1] == z2[N,M])
@NLconstraint(m,z3iv,z30[1] == z3[N,M])
@NLconstraint(m,z4iv,z40[1] == z4[N,M])
@NLconstraint(m,z5iv,z50[1] == z5[N,M])
@NLconstraint(m,z6iv,z60[1] == z6[N,M])
@NLconstraint(m,z7iv,z70[1] == 0)
@NLconstraint(m,ttiv,tt0[1] == 0)
@NLconstraint(m,Xiv[p in np],X0[p,1] == 0)

# - Demand constraint
@NLconstraint(m,Xfv[p in np],sum(((y[p,q]+1)/2)*X[p,Ni[q][end],M] for q in nq) >= D[p])

# - Transition must end at the end of st[q] element
@NLconstraint(m,μbtr[q in nq],μb[Ni[q][st[q]],M] == 
                                    sum(((y[p,q]+1)/2)*μp[p] for p in np))

# - Then, the state is forced to remain at this value along the rest of subinterval q
@NLconstraint(m,μtc[q in nq,i in Ni[q],j in mj;i>=Ni[q][st[q]]+1],μb[i,j] ==
                                                                  μb[Ni[q][st[q]],M])

# - Low order representation of u(t) along the transition time plus one element
@NLconstraint(m,u1c[q in nq,i in Ni[q][1]:(Ni[q][st[q]]+1),j in mj,j!=1],
                                                                  u1[i,j]==u1[i,1])
@NLconstraint(m,u2c[q in nq,i in Ni[q][1]:(Ni[q][st[q]]+1),j in mj,j!=1],
                                                                  u2[i,j]==u2[i,1])

# - Assignment constraints
@NLconstraint(m,ass1[q in nq],sum(y[p,q] for p in np) == 2 - P)
@NLconstraint(m,ass2[p in np],sum(y[p,q] for q in nq) == 2 - Q)

# - Summation of hi's
@NLconstraint(m,sumh[q in nq],sum(h[i] for i in Ni[q]) == 1)
