#  This Julia file generates the plots with the results obtained
#   by executing the main file

using PyPlot

close("all")
# Time vector
t = Array{Float64}(undef,N*M)
for i in 1:N
	for j in 1:M
      if i == 1
         tpre = 0
      else
         tpre = t[M*(i-1)]
      end
      t[j+M*(i-1)] = τ[j]*hs[i] + tpre
	end
end

# ========= The values at the beginning of each finite element are added ========
# - New Arrays to allocate the values
U1s = Array{Float64}(undef,N*M+N)
U2s = Array{Float64}(undef,N*M+N)
TTs = Array{Float64}(undef,N*M+N)
Z3s = Array{Float64}(undef,N*M+N)
Z4s = Array{Float64}(undef,N*M+N)
Z5s = Array{Float64}(undef,N*M+N)
Ss = Array{Float64}(undef,N*M+N-1)
x1s = Array{Float64}(undef,N*M+N)
x2s = Array{Float64}(undef,N*M+N)
x3s = Array{Float64}(undef,N*M+N)
x4s = Array{Float64}(undef,N*M+N)

# S domain 
for i in 2:N
   Ss[(i-1)*M+(i-1)] = t[(i-1)*M]
end
for i in ni
   for j in mj
      Ss[(M+1)*(i-1)+j] = t[M*(i-1)+j]
   end
end
pushfirst!(Ss,0.0)

# - The values at the beginning of each finite element are allocated first
for i in ni
   U1s[(i-1)*M+i] = u10s[i]
   U2s[(i-1)*M+i] = u20s[i]
   Z3s[(i-1)*M+i] = z30s[i]
   Z4s[(i-1)*M+i] = z40s[i]
   Z5s[(i-1)*M+i] = z50s[i]
   TTs[(i-1)*M+i] = tt0s[i]
   x1s[(i-1)*M+i] = X10s[i]
   x2s[(i-1)*M+i] = X20s[i]
   x3s[(i-1)*M+i] = X30s[i]
   x4s[(i-1)*M+i] = X40s[i]
end

# - then the values at the all collocation points are added
for i in ni
   for j in mj
      U1s[M*(i-1)+i+j] = u1s[M*(i-1)+j]
      U2s[M*(i-1)+i+j] = u2s[M*(i-1)+j]
      Z3s[M*(i-1)+i+j] = z3s[M*(i-1)+j]
      Z4s[M*(i-1)+i+j] = z4s[M*(i-1)+j]
      Z5s[M*(i-1)+i+j] = z5s[M*(i-1)+j]
      TTs[M*(i-1)+i+j] = tts[M*(i-1)+j]
      x1s[M*(i-1)+i+j] = X1s[M*(i-1)+j]
      x2s[M*(i-1)+i+j] = X2s[M*(i-1)+j]
      x3s[M*(i-1)+i+j] = X3s[M*(i-1)+j]
      x4s[M*(i-1)+i+j] = X4s[M*(i-1)+j]
   end
end
μBs = Z4s./Z3s

# ================================================================


plt1 = subplots()
subplot(321)
plot(Ss,μBs,linewidth=2)
xlabel(L"s",fontsize=14)
ylabel(L"\bar{μ}, kg/kmol",fontsize=12)
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)

subplot(322)
plot(Ss,Z5s,linewidth=2)
xlabel(L"s",fontsize=14)
ylabel(L"T, K",fontsize=12)
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)

subplot(323)
plot([0;Ss],[U1s[end];U1s],linewidth=2)
xlabel(L"s",fontsize=14)
ylabel(L"F_I, m^3/h",fontsize=12)
axhline(y=u1U,linewidth=1.5,linestyle="--")
axhline(y=u1L,linewidth=1.5,linestyle="--")
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)

subplot(324)
plot([0;Ss],[U2s[end];U2s],linewidth=2)
xlabel(L"s",fontsize=14)
ylabel(L"F_{cw}, m^3/h",fontsize=12)
axhline(y=u2U,linewidth=1.5,linestyle="--")
axhline(y=u2L,linewidth=1.5,linestyle="--")
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)

subplot(325)
plot(Ss,x1s,Ss,x2s,Ss,x3s,Ss,x4s,linewidth=2)
xlabel(L"s",fontsize=14)
ylabel(L"m^3",fontsize=12)
legend([L"m_1",L"m_2",L"m_3",L"m_4"],fontsize=12)
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)


tight_layout()
savefig("SolinS.pdf")
