# This Julia file contains the solution of the integrated optimal control
# and scheduling case study I: A simple CSTR with three products
#
# Scenario 1: No due dates
#
# Oswaldo Andrés-Martínez
# oswxandres@gmail.com
# Jan 2021
#
# Notes: There are chagnes in the nomenclature with respect to the paper
#        i and j are indices for finite elements and collocation points resp.
#        q is the index of subintervals
#        X's are the cumulative volume of each product   
#
using JuMP, Ipopt, PyPlot

# - Steady states
zp = [0.2 0.15 0.06] 
up = [298.65 299.54 313.64]
Tp = [374.32 379.76 395.82]


# Initial values
Xin = [0 0 0] 

# Model parameters
qv  = 1    # q/V (1/h)
ER  = 1e4  # E/R (K)
UA  = 1	  # UA/VρCp (1/h)
k0  = 1e12 # Rate constant (1/h)
T0  = 350  # Feed temperature (K)
CA0 = 0.7  # Feed concentration (mol/l)
ΔHρ = -200 # ΔH/ρCp Kl/mol

# Bounds on u(t)
uL = 250
uU = 370

# Scheduling parameters
Cr = 20              # Cost of raw material ($/m3)
qf = 1               # Feed flow rate (m3/h)
D = [10 30 10]       # Demand of products (m3)
Cp = [100 120 200]   # Prices of each product ($/m3)
Cs = [20 2 3]        # Storage costs ($/m3h)

# Other parameters
δ = 1e3 #  Penalty parameter

# Position of points sj^t
st = [2 2 2]  
# Off-spec and control effort costs
α1 = 0
α2 = 0
α3 = 1

# Size parameters
N = 12  # Number of finite elements
M = 2   # Number of collocation points
P = 3   # Number of products
Q = 3   # Number of subintervals

# Collocation matrix and points and quadrature weights 
# (Radau points)
include("Rtableau.jl")
Ω,ω,τ = butcher(M+1)
# Lagrange coefficients for interpolation
include("Lagrange.jl")
lag = Lagrange(0,τ)

# Sets
np = 1:P  # Products
nq = 1:Q  # Intervals
ni = 1:N  # Finite elements
Ni = Array{UnitRange}(undef,Q)  # Subintervals set
for i in 1:Q
   Ni[i] = ((i-1)*Int(N/Q) + 1):i*Int(N/Q)
end
mj = 1:M  # Collocation points
mk = copy(mj)

# Initial guess
# - Guessed sequence
Seq0 = [2 3 1]
include("initial_guess.jl")

# Generate the nlp 
include("cstr.jl")

# Solution
optimize!(m)
  
# Reading results
hs = value.(h).data
include("results.jl")

# Plots in the domain s
include("plots1.jl")

# Plots in the domain t
include("plots2.jl")
