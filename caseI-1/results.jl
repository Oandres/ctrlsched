# Auxiliary file to read the optimal result from the solution
z1s = transpose(value.(z1).data)[:]
z2s = transpose(value.(z2).data)[:]
z3s = transpose(value.(z3).data)[:]
X1s = transpose(value.(X).data[1,:,:])[:]
X2s = transpose(value.(X).data[2,:,:])[:]
X3s = transpose(value.(X).data[3,:,:])[:]
z10s = value.(z10).data
z20s = value.(z20).data
X10s = value.(X0).data[1,:]
X20s = value.(X0).data[2,:]
X30s = value.(X0).data[3,:]
tt0s = value.(tt0).data
us = transpose(value.(u).data)[:]
u0s = value.(u0).data
ys = transpose(value.(y).data)[:]
vs = value.(v).data
tts = transpose(value.(tt).data)[:]
Jschs = value(Jsch)
Jdyns = value(Jdyn)
Jpens = value(Jpen)
Jtot  = Jschs-Jdyns
tsw1 = value(tt[Ni[1][end],M])
tsw2 = value(tt[Ni[2][end],M])
cputime = solve_time(m)
println("Jsch: ",Jschs)
println("Jdyn: ",Jdyns)
println("Jpen: ",Jpens)
println("Jtot: ",Jtot)
println("Final time = ",tts[end])
println("Switching times : ")
println("t1: ",tsw1)
println("t2: ",tsw2)
println("CPU time = ",cputime)
open("solution.txt","w") do g
   write(g,"Jsch = $Jschs\n")
   write(g,"Jdyn = $Jdyns\n")
   write(g,"Jtot = $Jtot\n")
   write(g,"Final time = $(tts[end])\n")
   write(g,"Switching times : \n")
   write(g,"t1: $tsw1\n")
   write(g,"t2: $tsw2\n")
   write(g,"CPU time: $cputime\n")
end 

