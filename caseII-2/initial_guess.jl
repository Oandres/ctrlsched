# This julia file generates initial guess values for z,u and y based on
# the sequence Seq0 

Seqi = Dict{Int64,Int64}()
for p in np
   Seqi[p] = Seq0[p]
end

Seqj = sort(collect(Seqi),by=x->x[1])

Z10 = Array{Float64}(undef,N,M)
Z20 = Array{Float64}(undef,N,M)
U0 = Array{Float64}(undef,N,M)
for q in nq
   k = Seqj[q][2]
   Z10[Ni[q],:] .= zp[k]
   Z20[Ni[q],:] .= Tp[k]
   U0[Ni[q],:] .= up[k]
end

x0 = Array{Float64}(undef,P,N,M)
x0 .= 0.0
for q in nq
   k = Seqj[q][2]
   x0[k,Ni[q][1]:end,:] .= D[k]
end   

y0 = Array{Float64}(undef,P,Q)
y0 .= -1.0
for q in nq
   k = Seqj[q][2]
   y0[k,q] = 1.0
end  


