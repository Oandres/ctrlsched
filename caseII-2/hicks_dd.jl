# This Julia file contains the model of the integrated optimal control
# and scheduling case study II: Hicks CSTR
#
# Scenario 2: Due date on i=1
#
# Oswaldo Andrés-Martínez
# oswxandres@gmail.com
# Jan 2021
#
# Updated Feb 2021  

using JuMP, Ipopt, PyPlot

# Model definition
m = Model(Ipopt.Optimizer)

# Variables
@variable(m,z1[i in ni,j in mj],start=Z10[i,j]) 
@variable(m,z2[i in ni,j in mj],start=Z20[i,j])   
@variable(m,z3[i in ni,j in mj])              
@variable(m,v[q in nq]>=0,start=10)
@variable(m,0<=X[p in np,i in ni,j in mj]<= 1.1D[p],start=x0[p,i,j])
@variable(m,z10[i in ni],start=Z10[i,1])
@variable(m,z20[i in ni],start=Z20[i,1])
@variable(m,z30[i in ni])
@variable(m,-1<=y[p in np,q in nq]<=1,start=y0[p,q])
@variable(m,X0[p in np,i in ni]>=0,start=x0[p,i,1])
@variable(m,tt[i in ni,j in mj]>=0)
@variable(m,tt0[i in ni]>=0)
@variable(m,uL <= u[i in ni,j in mj] <= uU,start=U0[i,j])
@variable(m,0.1Q/N <= h[i in ni] <= 2Q/N,start=Q/N)
# ================  Auxiliary expressions ============
# - Penalty function
function γp(x)
   return log(0.5,x)
end
register(m,:γp,1,γp,autodiff=true)

# - Due date function
function Dd1(dt)
   if dt >= td1
      return D[1]
   else
      return 0.0
   end
end
register(m,:Dd1,1,Dd1,autodiff=true)



# - Deviation from the steady state
@NLexpression(m,SS[p in np,i in ni,j in mj],α1*(zp[p]-z1[i,j])^2
                         + α2*(Tp[p]-z2[i,j])^2 + α3*(up[p]-u[i,j])^2)

# - RHS of the state equations
@NLexpression(m,f1[i in ni,j in mj],(1-z1[i,j])/θ - k10*exp(-n/z2[i,j])*z1[i,j])
@NLexpression(m,f2[i in ni,j in mj],(yf-z2[i,j])/θ + k10*exp(-n/z2[i,j])*z1[i,j]
                                        - α*u[i,j]*(z2[i,j] - yc))
@NLexpression(m,f3[q in nq,i in Ni[q],j in mj],
                sum(((y[p,q]+1)/2)*SS[p,i,j] for p in np))
@NLexpression(m,f4[p in np,q in nq,i in ni,j in mj],(v[q]*(y[p,q]+1)/2)*qf)

# - Interpolation of algebraic variables
@NLexpression(m,u0[i in ni],sum(lag[j]*u[i,j] for j in mj))


# - Collocation equations
#   RHS of the state equations in the domain s
@NLexpression(m,z1dot[q in nq,i in Ni[q],j in mj],v[q]*f1[i,j])
@NLexpression(m,z2dot[q in nq,i in Ni[q],j in mj],v[q]*f2[i,j])
@NLexpression(m,z3dot[q in nq,i in Ni[q],j in mj],v[q]*f3[q,i,j])
@NLexpression(m,Xdot1[p in np,q in nq,i in Ni[q],j in mj;i<Ni[q][st[q]]+1],0)
@NLexpression(m,Xdot2[p in np,q in nq,i in Ni[q],j in mj;i>=Ni[q][st[q]]+1],f4[p,q,i,j])
#   Equation for t(s)
@NLexpression(m,tdot[q in nq,i in Ni[q],j in mj],v[q])

# - Economic expresion for the objective function
@NLexpression(m,Jp[q in nq,i in Ni[q],j in mj],sum(δ*γp(y[p,q]^2) for p in np))
@NLexpression(m,Jpint[q in nq],sum(h[i]*ω[j]*v[q]*Jp[q,i,j] for i in Ni[q],j in mj))
@NLexpression(m,Jpen,sum(Jpint[q] for q in nq))

# - inventory level
@NLexpression(m,IL1[i in ni,j in mj],X[1,i,j] - Dd1(tt[i,j]))
@NLexpression(m,IL2[i in ni,j in mj],X[2,i,j])
@NLexpression(m,IL3[i in ni,j in mj],X[3,i,j])
@NLexpression(m,IL4[i in ni,j in mj],X[4,i,j])
@NLexpression(m,inv1[q in 1:Q-1],((y[1,q]+1)/2)*sum(h[i]*ω[j]*v[q2]*IL1[i,j] 
                                 for q2 in (q+1):Q,i in Ni[q2],j in mj)) 
@NLexpression(m,inv2[q in 1:Q-1],((y[2,q]+1)/2)*sum(h[i]*ω[j]*v[q2]*IL2[i,j] 
                                 for q2 in (q+1):Q,i in Ni[q2],j in mj)) 
@NLexpression(m,inv3[q in 1:Q-1],((y[3,q]+1)/2)*sum(h[i]*ω[j]*v[q2]*IL3[i,j] 
                                 for q2 in (q+1):Q,i in Ni[q2],j in mj)) 
@NLexpression(m,inv4[q in 1:Q-1],((y[4,q]+1)/2)*sum(h[i]*ω[j]*v[q2]*IL4[i,j] 
                                 for q2 in (q+1):Q,i in Ni[q2],j in mj)) 

# - Jsch
@NLexpression(m,Jsch,sum(Cp[p]*((y[p,q]+1)/2)*X[p,Ni[q][end],M] for p in np,q in nq) 
 - sum(Cs[1]*inv1[q] for q in 1:Q-1) - sum(Cs[2]*inv2[q] for q in 1:Q-1)
 - sum(Cs[3]*inv3[q] for q in 1:Q-1) - sum(Cs[4]*inv4[q] for q in 1:Q-1) - qf*Cr*tt[N,M])

# - Jdyn
@NLexpression(m,Jdyn,sum(z3[Ni[q][end],M] for q in nq))
# =====================================================================


# Objective function
@NLobjective(m,Max,Jsch - Jdyn - Jpen)

# Constraints
# - RK representation of the state and adjoint equations
@NLconstraint(m,h1[q in nq,i in Ni[q],j in mj],
      z1[i,j] == z10[i]+h[i]*sum(Ω[j,k]*z1dot[q,i,k] for k in mk))
@NLconstraint(m,h2[q in nq,i in Ni[q],j in mj],
      z2[i,j] == z20[i]+h[i]*sum(Ω[j,k]*z2dot[q,i,k] for k in mk))
@NLconstraint(m,h3[q in nq,i in Ni[q],j in mj],
      z3[i,j] == z30[i]+h[i]*sum(Ω[j,k]*z3dot[q,i,k] for k in mk))
@NLconstraint(m,h41[p in np,q in nq,i in Ni[q],j in mj;i<Ni[q][st[q]]+1],
      X[p,i,j] == X0[p,i]+h[i]*sum(Ω[j,k]*Xdot1[p,q,i,k] for k in mk))
@NLconstraint(m,h42[p in np,q in nq,i in Ni[q],j in mj;i>=Ni[q][st[q]]+1],
      X[p,i,j] == X0[p,i]+h[i]*sum(Ω[j,k]*Xdot2[p,q,i,k] for k in mk))
@NLconstraint(m,h5[q in nq,i in Ni[q],j in mj],
      tt[i,j] == tt0[i]+h[i]*sum(Ω[j,k]*tdot[q,i,k] for k in mk))

# - Continuity
@NLconstraint(m,c1[i in 2:N],z10[i] == z1[i-1,M])
@NLconstraint(m,c2[i in 2:N],z20[i] == z2[i-1,M])
@NLconstraint(m,c3[i in 2:N],z30[i] == z3[i-1,M])
@NLconstraint(m,c4[p in np,i in 2:N],X0[p,i] == X[p,i-1,M])
@NLconstraint(m,c5[i in 2:N],tt0[i] == tt[i-1,M])

# - Boundary values
@NLconstraint(m,z1iv,z10[1] == z1[N,M])
@NLconstraint(m,z2iv,z20[1] == z2[N,M])
@NLconstraint(m,z3iv,z30[1] == 0)
@NLconstraint(m,ttiv,tt0[1] == 0)
@NLconstraint(m,Xiv[p in np],X0[p,1] == 0)

# - Due date 
@NLconstraint(m,dd1,sum(((y[1,q]+1)/2)*tt[Ni[q][end],M] for q in nq) <= td1)

# - Demand constraint
@NLconstraint(m,Xfv[p in np],sum(((y[p,q]+1)/2)*X[p,Ni[q][end],M] for q in nq) >= D[p])

# - Transition must end at the end of st[q] element
@NLconstraint(m,z1tr[q in nq],z1[Ni[q][st[q]],M] == 
                                    sum(((y[p,q]+1)/2)*zp[p] for p in np))

# - Then, the state is forced to remain at this value along the rest of subinterval q
@NLconstraint(m,z1tc[q in nq,i in Ni[q],j in mj;i>=Ni[q][st[q]]+1],z1[i,j] ==
                                                                  z1[Ni[q][st[q]],M])

# - Low order representation for u
@NLconstraint(m,uc[q in nq,i in Ni[q][1]:(Ni[q][st[q]]+1),j in mj;j!=1],
                                                            u[i,1] == u[i,j])

# - Assignment constraints
@NLconstraint(m,ass1[q in nq],sum(y[p,q] for p in np) == 2 - P)
@NLconstraint(m,ass2[p in np],sum(y[p,q] for q in nq) == 2 - Q)

# - Summation of hi's
@NLconstraint(m,sumh[q in nq],sum(h[i] for i in Ni[q]) == 1)
