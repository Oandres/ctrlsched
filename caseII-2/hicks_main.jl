# This Julia file contains the solution of the integrated optimal control
# and scheduling case study II: Hicks CSTR
#
# Scenario 2: Due date on i=1
#
# Oswaldo Andrés-Martínez
# oswxandres@gmail.com
# Jan 2021
#
# Notes: There are chagnes in the nomenclature with respect to the paper
#        i and j are indices for finite elements and collocation points resp.
#        q is the index of subintervals
#        X's are the cumulative volume of each product 
# Updated Feb 2021  

using JuMP, Ipopt, PyPlot

# - Steady states
zp = [0.0944 0.1367 0.1926 0.2632] 
up = [340 390 430 455]
Tp = [0.7766 0.7292 0.6881 0.6519]

# Initial values
Xin = [0 0 0 0] # Initial amount of products (m3)

# Model parameters
α  = 1.95e-4  # Dimensionless parameter
k10 = 300 	  # Rate constant
n = 5
θ = 20
yf = 0.3947
yc = 0.3816

# Bounds on u(t)
uL = 0
uU = 700

# Scheduling parameters
Cr = 10 # Cost of raw material ($/m3)
qf = 1.1 # Feed flow rate (m3/h)
D = [10.0 20.0 20.0 15.0]       # Demand of products (m3)
Cp = [180 110 60 140]   # Prices of each product ($/m3)
Cs = [5.0 0.5 2.2 1.05]        # Storage costs ($/m3h)
td1 = 48.0           # Due date for product 1

# Other parameters
δ = 1e3 #  Penalty parameter

# Position of points sj^t 
st = [2 2 2 2]  

# Off-spec and control effort costs
α1 = 1e-1  
α2 = 1e-1 
α3 = 1e-4   

# Size parameters
N = 16  # Number of finite elements
M = 2   # Number of collocation points
P = 4   # Number of products
Q = 4   # Number of intervals

# Collocation matrix and points and quadrature weights 
# (Radau points)
include("Rtableau.jl")
Ω,ω,τ = butcher(M+1)
# Lagrange coefficients for interpolation
include("Lagrange.jl")
lag = Lagrange(0,τ)

# Sets
np = 1:P  # Products
nq = 1:Q  # Intervals
ni = 1:N
Ni = Array{UnitRange}(undef,Q)  # Subintervals set
for i in 1:Q
   Ni[i] = ((i-1)*Int(N/Q) + 1):i*Int(N/Q)
end
mj = 1:M  # Collocation points
mk = copy(mj)

# Initial guess
# - Guessed sequence
Seq0 = [2 1 4 3]
include("initial_guess.jl")

# Generate the nlp 
include("hicks_dd.jl")

# Solution
optimize!(m)
  
# Reading results
hs = value.(h).data
include("results.jl")

# Plots in the domain s
include("plots1.jl")

# Plots in the domain t
include("plots2.jl")
